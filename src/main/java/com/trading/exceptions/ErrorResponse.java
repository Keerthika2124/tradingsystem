package com.trading.exceptions;

import org.springframework.http.HttpStatus;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorResponse {

	private HttpStatus code;
	private String message;
}
