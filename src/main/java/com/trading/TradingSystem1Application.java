package com.trading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradingSystem1Application {

	public static void main(String[] args) {
		SpringApplication.run(TradingSystem1Application.class, args);
	}

}
