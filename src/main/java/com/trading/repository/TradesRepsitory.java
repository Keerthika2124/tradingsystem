package com.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trading.entity.Trades;

public interface TradesRepsitory extends JpaRepository<Trades, Long> {

}
