package com.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trading.entity.UserDetails;

public interface UserdetailsRepository extends JpaRepository<UserDetails, Long> {

}
