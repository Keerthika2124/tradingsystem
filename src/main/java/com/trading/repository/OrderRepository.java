package com.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trading.entity.OrderDetails;

public interface OrderRepository extends JpaRepository<OrderDetails, Long> {

}
