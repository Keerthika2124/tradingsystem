package com.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trading.entity.LoginDetails;

public interface LoginRepository extends JpaRepository<LoginDetails, Long> {

}
