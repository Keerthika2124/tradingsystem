package com.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trading.entity.AccountDetails;

public interface AccountdetailsRepository extends JpaRepository<AccountDetails, Long> {

}
