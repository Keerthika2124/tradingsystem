package com.trading.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trading.entity.StockPrice;

public interface StockpriceRepository extends JpaRepository<StockPrice, Long> {

}
