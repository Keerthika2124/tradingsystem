package com.trading.entity;

import java.time.LocalDate;

import com.trading.dto.OrderType;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Trades {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long tradeId;
	private int units;
	private LocalDate buyDate;
	private double costPrice;
	private double totalPrice;
	
	@OneToOne
	@JoinColumn(name="stock_id")
	private Stocks stocks;
	
	@OneToOne
	@JoinColumn(name="user_details_id")
	private UserDetails userDetails;

}
