package com.trading.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StockPrice {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long spId;
	
	@ManyToOne
    @JoinColumn(name = "stockId")
    private Stocks stocks;
	private String stockname;
	private LocalDate currentdate;
 

}
