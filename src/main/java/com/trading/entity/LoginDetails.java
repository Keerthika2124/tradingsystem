package com.trading.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginDetails {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long loginId;
	private LocalDate loginDate;
	private boolean status;
	
	@OneToOne
	@JoinColumn(name="user_details_id")
	private UserDetails userDetails;
	
}
