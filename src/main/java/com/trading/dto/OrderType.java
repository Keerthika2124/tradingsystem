package com.trading.dto;

public enum OrderType {

	BUY, SELL;
}
